/*
 * @Author: hexiaotian
 * @Date:   2017-01-16 16:08:53
 * @Last Modified by: hexiaotian
 * @Last Modified time: 2018-07-11 10:10:12
 */
var express = require('express');
var router = express.Router();
module.exports = router;
var info = require('../../core/info/index').info;


// 资讯列表
router.get('/', info.getInfoList, info.getInfoAdverts, function(req, res) {
    res.render('info/index', req.body);
});

// 资讯翻页
router.get('/getInfoList', info.getInfoList, function(req, res) {
    res.json(req.body);
});

// 详情
router.get('/:id', info.getInfoDetail, function(req, res) {
    res.render('info/detail', req.body);
});