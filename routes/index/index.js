/*
 * @Author: hexiaotian
 * @Date:   2017-01-16 16:08:53
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-07-28 17:41:18
 */
var express = require('express');
var router = express.Router();
var home = require('../../core/index/index').home;
module.exports = router;


router.get('/',  home.getTypeList, home.getImages, home.getNewsList, home.getPlaylists,function(req, res) {
    res.render('index/index',req.body);
});

 //获取正片/花絮列表
router.post('/getPlayer', home.getVideos,function(req, res) {
    res.json(req.body.getVideos);
});

 //获取广告列表
router.post('/getAdverts', home.getAdverts,function(req, res) {
    res.json(req.body.getAdverts);
});
// 选手（包含导师、教练、嘉宾与普通选手
router.post('/getPeople', home.getPlayer,function(req, res) {
    res.json(req.body.getPlayer);
});


