/*
 * @Author: hexiaotian
 * @Date:   2017-01-16 16:08:53
 * @Last Modified by: hexiaotian
 * @Last Modified time: 2018-07-11 10:10:12
 */
var express = require('express');
var router = express.Router();
module.exports = router;
var fragment = require('../../core/fragment/index').fragment;


router.get(['/', '/:id'], fragment.getFragmentDetails, fragment.getFragmentList, function(req, res) {
    res.render('fragment/index', req.body);
});
