/*
 * @Author: hexiaotian
 * @Date:   2017-01-16 16:08:53
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-08-15 20:48:45
 */
var express = require('express');
var router = express.Router();
module.exports = router;
var positive = require('../../core/positive/index').positive;


router.get(['/', '/:id'], positive.getPositiveDetails, positive.getPositiveList, function(req, res) {
    res.render('positive/index', req.body);
});

