/*
 * @Author: hexiaotian
 * @Date:   2017-01-16 16:08:53
 * @Last Modified by: hexiaotian
 * @Last Modified time: 2018-07-11 10:10:12
 */
var express = require('express');
var router = express.Router();
module.exports = router;
var search = require('../../core/search/index').search;


router.get('/', search.getSearchList, function(req, res) {
    res.render('search/index', req.body);
});

router.get('/getSearchList', search.getSearchList, function(req, res) {
    res.json(req.body);
});