(function ($) {

  var $main = $('#wrapper'),
    $pullUp = $('#pullUp'),
    $tab = $('#tab'),
    $listBox = $('#listBox'),
    $pullUpMsg = $('#pullUp-msg');

  var list = {
    isPC: false,
    page: 2,
    isLock: false, // 加载标记
    type: '',
    init: function () {
      if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(navigator.userAgent)) {
        // 移动端
        list.isPC = false;
        var isPulled = false; // 拉动标记
        ($pullUpMsg.text() == '加载更多') ? $pullUpMsg.text('上拉加载') : '';

        // 加载iscroll
        var myScroll = new IScroll('#wrapper', {
          mouseWheel: true, // 开启鼠标滚轮
          scrollbars: true, // 开启滚动条
          interactiveScrollbars: true, // 滚动条可拖到
          shrinkScrollbars: 'scale', // 滚动条按比例缩放
          fadeScrollbars: true, // 无滚动时隐藏滚动条
          probeType: 3 // eum...支持自定义onscroll事件
        });
        list.myScroll = myScroll;

        myScroll.on('scroll', function () {
          var height = this.y,
            bottomHeight = this.maxScrollY - height;
            // console.log(height, bottomHeight);
          // 控制上拉显示
          if (bottomHeight >= -216 && !list.isLock) {
            $pullUp.slideDown(500);
            isPulled = true;
            list.isLock = true;
            list.requestData(list.page);
            $pullUpMsg.text('加载中...');
            return;
          }
        })
       
      } else {
        // pc端
        list.isPC = true;
        myScroll && myScroll.destroy();
        $main.css('padding-top', $('.navbar').height());
        $pullUp .click(function() {
          $pullUpMsg.text('加载中...');
          list.requestData();
        })
      }
      
      //初始化tab
      list.initTab();
    },
    requestData: function () {
      
      getData('/search/getSearchList', 'GET', {
        'page': list.page,
        'title': $main.attr('data-title'),
        'type': list.type
      }).then(function (data) {
        console.log(data);
        if (data) {
          list.updateContent(data.data)
        }
      }).fail(function (err) {
        console.log(err);
      })
    },
    updateContent: function (data) {
      var html = ejs.render('<% for(var i=0, item; item = infoList[i++];){%>'+
				'<li class="clearfix  <%= item.type%>">'+
					'<a href="/<%= item.tabType == 0 ? "positive" : (item.tabType==1 ? "fragment" : "info")%>/<%= item.id%>">'+
            '<div class="box-left">'+
              '<img src="<%= item.images%>" alt="">'+
            '</div>'+
            '<div class="box-right">'+
              '<h5><%= item.title %></h5>'+
              '<p class="hidden-xs"><%= item.description %></p>'+
              '<p><%= formatDate(item.realseTime, "MM月DD日") %></p>'+
						'</div>'+
					'</a>'+
        '</li>'+
      '<%}%>', {
        infoList: data.data
      });
      
      $('#searchDec span').text(data.count);
      if (!data.count) {
        $('.nodata').addClass('block');
        $pullUp.addClass('hidden');
      } else {
        $pullUp.removeClass('hidden');
        $('.nodata').removeClass('block');
      }
      var msg = list.isPC ? '加载更多' : '上拉加载';
      if (data.data.length == 10) {
        list.isLock = false;
        $pullUpMsg.text(msg);
        list.page++;
      } else {
        $pullUp.hide();
      }
      $listBox.append(html);
      // 移动端刷新scroll
      if (!list.isPC) {
        setTimeout(function () {
          list.myScroll.refresh();
        }, 6);
      }
    },
    initTab: function () {
      
      $tab.find('li').click(function() {
        $pullUp.show();
        $(this).addClass('active').siblings().removeClass('active');
        list.type = $(this).attr('data-type');
        console.log(list.type);
        list.page = 1;
        $listBox.empty();
        list.requestData();
      })
    }
  }
  window.onload = function () {
    list.init();
  }
})(jQuery)