(function ($) {

  var $main = $('#wrapper'),
    $pullUp = $('#pullUp'),
    $tab = $('#tab'),
    $listBox = $('#listBox'),
    $pullUpMsg = $('#pullUp-msg');
    $lis = $('.video-box .clearfix li');

  var video = {
    isPC: false,
    // 视频点击获取对应播放来源
		player: function (myUrl) {
			// 下面的例子用来获取url的两个参数，并返回urlRewrite之前的真实Url
			setTimeout(function () {

				var reg = new RegExp('http://v.youku.com/v_show/id_([^.]*).html'),
					regs = new RegExp('https://v.youku.com/v_show/id_([^.]*).html'),
					url = myUrl;
				if (url.indexOf("https") != -1) {
					url = url.replace(regs, 'http://player.youku.com/embed/$1')
				} else {
					url = url.replace(reg, 'http://player.youku.com/embed/$1')
				}
				$('#iframe').attr('src', url + '?autoplay=1')
				// var reg = new RegExp('http://v.youku.com/v_show/id_([^.]*).html')
				// var regs = new RegExp('https://v.youku.com/v_show/id_([^.]*).html')
			
				// if (!!url) {
				// 	//判断传过来的是iframe标签还是网址链接
				// 	var getIframe = /^[ ]*<iframe/
				// 	var isIframe = getIframe.test(url)
				// 	if (isIframe) {
				// 		var replaceString = /'allowfullscreen'/
				// 		$('.iframe').html(url.replace(replaceString, ''))
				// 	} else {
				// 		//iframe标签 [图片]http://player.youku.com/embed/XMjYxNDYwODM1Mg==
				// 		if (url.indexOf("https") > -1) {
				// 			var a = url.replace(reg, 'https://player.youku.com/embed/$1');
				// 			if (regs.test(url)) {
				// 				$('.iframe').attr('src', a + '?autoplay=0')
				// 			}
				// 		} else {
				// 			var a = url.replace(reg, 'http://player.youku.com/embed/$1');
				// 			if (reg.test(url)) {
				// 				$('.iframe').attr('src', a + '?autoplay=0')
				// 			}
				// 		}
				// 	}
				// }
			}, 300)
		},
    init: function () {
      if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(navigator.userAgent)) {
        // 移动端
        video.isPC = false;
       
      } else {
        $main.css('padding-top', $('.navbar').height());
      }

     
        var urlParams = getLastString(location.href);
      
        for(var i=0; i < $lis.length; i++ ){
          
          if($lis.eq(i).attr('data-videoId') == urlParams){
             
              video.player($lis.eq(i).attr('data-schemurl'));
              $lis.eq(i).addClass('active').siblings().removeClass("active")
          }
        }
   
        $lis.on('click', function(){
          video.player($(this).attr('data-schemurl'));
		      $(this).addClass('active').siblings().removeClass("active")
        })
      

    }
  }
  window.onload = function () {
    video.init();
  }
})(jQuery)