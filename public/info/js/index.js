(function ($) {

  var $main = $('#wrapper'),
    $pullUp = $('#pullUp'),
    $tab = $('#tab'),
    $listBox = $('#listBox'),
    $pullUpMsg = $('#pullUp-msg');

  var list = {
    isPC: false,
    page: 2,
    isLock: false, // 加载标记
    type: '',
    init: function () {
      if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(navigator.userAgent)) {
       
        // 移动端
        list.isPC = false;
        var isPulled = false; // 拉动标记
        ($pullUpMsg.text() == '加载更多') ? $pullUpMsg.text('上拉加载') : '';

      

        // 加载iscroll
          var myScroll = new IScroll('#wrapper', {
            mouseWheel: true, // 开启鼠标滚轮
            scrollbars: true, // 开启滚动条
            interactiveScrollbars: true, // 滚动条可拖到
            shrinkScrollbars: 'scale', // 滚动条按比例缩放
            fadeScrollbars: true, // 无滚动时隐藏滚动条
            probeType: 3, // eum...支持自定义onscroll事件
            click: true
          });
          list.myScroll = myScroll;

          myScroll.on('scroll', function () {
            var height = this.y,
              bottomHeight = this.maxScrollY - height;
              // console.log(height, bottomHeight);
            // 控制上拉显示
            if (bottomHeight >= -216 && !list.isLock) {
              $pullUp.slideDown(500);
              isPulled = true;
              list.isLock = true;
              list.requestData(list.page);
              $pullUpMsg.text('加载中...');
              return;
            }
          })
        
          
      


       
      } else {
        // pc端
        list.isPC = true;
        $pullUpMsg.text('加载更多数据')
        myScroll && myScroll.destroy();
        $main.css('padding-top', $('.navbar').height());
        $pullUp.click(function() {
          var msg = $pullUpMsg.text();
          $pullUpMsg.text('加载中...');
          list.requestData();
        })
      }
    },
    requestData: function () {
      console.log(list.page, $main.attr('data-title'));
      
      getData('/info/getInfoList', 'GET', {
        'page': list.page
        // 'title': $main.attr('data-title')
        // 'type': list.type
      }).then(function (data) {
        console.log(data);
        if (data) {
          list.updateContent(data.data)
        }
      }).fail(function (err) {
        console.log(err);
      })
    },
    updateContent: function (data) {
      var html = ejs.render('<% for(var i=0, item; item = infoList[i++];){%>' +
				'<a href="/info/<%= item.id%>">' +
          '<li class="clearfix  <%= item.type%>">' +
            '<div class="box-left">' +
              '<img src="<%= item.images %>" alt="">' +
            '</div>' +
            '<div class="box-right">' +
              '<h5><%= item.title %></h5>' +
              '<p class="hidden-xs"><%= item.description %></p>' +
              '<p><%= formatDate(item.realseTime && item.realseTime.replace(".000+0000",""), "MM月DD日") %></p>' +
            '</div>' +
         '</li>' +
				'</a>' +
        '<%}%>', {
        infoList: data.data
      });
      var msg = list.isPC ? '加载更多数据' : '上拉加载';
      if (data.data.length >= 10) {
        list.isLock = false;
        $pullUpMsg.text(msg);
        list.page++;
      } else {
        $pullUp.hide();
      }
      $listBox.append(html);
      // 移动端刷新scroll
      if (!list.isPC) {
        setTimeout(function () {
          list.myScroll.refresh();
        }, 6);
      }
    }
  }
  window.onload = function () {
    list.init();
  }
})(jQuery)