
// 滑动按钮隐显封装
function isBlock (btnr, btnl, n, num) {

	if (n >= num) {
		btnr.css({
			display: 'none'
		})
		btnl.css({
			display: 'block'
		})
	} else if (n == 0) {
		btnl.css({
			display: 'none'
		})
		btnr.css({
			display: 'block'
		})
	} else {
		btnl.css({
			display: 'block'
		})
		btnr.css({
			display: 'block'
		})
	}
};
// 滑动功能封装
function btnshow (idsLen, btnr, btnl, sliderUL, distance, imgIndex) {
	// idsLen滑动所有的li，btnr右按钮，btnl左按钮，sliderUL为ul,num滑动距离
	var liNum = distance / sliderUL.find('li').width();
	var num = idsLen / liNum;
	var n = 0;
	
	if (imgIndex) {
		n = Math.floor((imgIndex) / Math.floor(liNum));
	}

	sliderUL.css({
		left: -distance * n + 'px',
		transition: 'all 0.5s'
	})

	isBlock(btnr, btnl, n, num)
	btnr.on('click', function () {

		++n
		sliderUL.css({
			left: -distance * n + 'px',
			transition: 'all 0.5s'
		})
		isBlock(btnr, btnl, n, num)
	})
	btnl.on('click', function () {
		--n
		sliderUL.css({
			left: -distance * n + 'px',
			transition: 'all 0.5s'
		})
		isBlock(btnr, btnl, n, num)
	})
};

// 显示隐藏点击按钮
function btnHide(btn,num){
	
	
	// btn为按钮，num为第几张图片
	if(btn.eq(0).hasClass('viewer-active') && btn.eq(btn.length-1).hasClass('viewer-active')){
		$('.viewer-toolbar .viewer-prev').hide()
		$('.viewer-toolbar .viewer-next').hide()
	}else{
			if(num == 0){
				setTimeout(function(){
					if(btn.eq(0).hasClass('viewer-active')){
						$('.viewer-toolbar .viewer-prev').hide()
						$('.viewer-toolbar .viewer-next').show()
					}else{
						$('.viewer-toolbar .viewer-prev').show()
						$('.viewer-toolbar .viewer-next').show()
					}
				},500)
			}else if(num == btn.length-1){
				
				setTimeout(function(){
					if(btn.eq(num).hasClass('viewer-active')){
						$('.viewer-toolbar .viewer-prev').show()
						$('.viewer-toolbar .viewer-next').hide()
					}else{
						$('.viewer-toolbar .viewer-prev').show()
						$('.viewer-toolbar .viewer-next').show()
					}
				},500)
				
			}else{
				$('.viewer-toolbar .viewer-prev').show()
				$('.viewer-toolbar .viewer-next').show()
			}
			
		
	}	
}
window.onload = function(){
	$('#dowebok').viewer({
		title: false,
		tooltip: false,
		movable: false,
		zoomable: false,
		rotatable: false,
		scalable: false,
		zoomRatio: 0.00001,
		tooltip: false,
		loop: false,
		built: function() {
			if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(
				navigator.userAgent)) {

			} else {
				$('.viewer-navbar').append('<div class="viewer-list-left" id="viewerListLeft"></div><div class="viewer-list-right" id="viewerListRight"></div>');
			}
		},
		shown: function() {
			if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(
				navigator.userAgent)) {
				
					$('.viewer-navbar').scrollLeft($('.viewer-list .viewer-active').index()*$('.viewer-list .viewer-active').width())
			} else {
				// 图片查看器 翻页
				btnshow($('.viewer-list li').length, $('#viewerListRight'), $(
					'#viewerListLeft'), $('.viewer-navbar .viewer-list'), $('.viewer-navbar').width()-80, $('.viewer-list .viewer-active').index());

					if ($('.viewer-list li').eq(0).hasClass('viewer-active')) {
						$('.viewer-toolbar .viewer-prev').hide()
					} else if ($('.viewer-list li').eq(-1).hasClass('viewer-active')){
						$('.viewer-toolbar .viewer-next').hide()
					}
					
					$('.viewer-prev').on('click',function(){
						$(this).addClass('active')						
						btnHide($('.viewer-list li'),0)
						
					})
					$('.viewer-next').on('click',function(){						
						$(this).addClass('active')
						btnHide($('.viewer-list li'),$('.viewer-list li').length-1)
					})
					$('.viewer-list li').on('click',function(){						
						btnHide($('.viewer-list li'),$(this).index())
					})
			}
		}
		
	});
};
(function ($) {
	var list = {
		isPC: false,
		init: function () {

			// 视频点击获取对应播放来源
			list.player($('.dataList .item').eq(0).attr('data-schemurl'));
			$('.dataList .item').eq(0).addClass('active').siblings().removeClass("active")
			

			// 获取正片
			list.getPlayer(0, function (res) {
				var data = res;
				var lis = ejs.render('<% for(var i=0, item; item = infoList[i++];){%><li class="con-list-li"><a href="/positive/<%= item.id%>" target="_blank"><div class="con-piclist-pic"><img src="<%= item.images %>" alt=""></div><div class="con-piclist-info"><a href="/positive/<%= item.id%>" class="con-piclist-title" target="_blank"><div class="con-piclist-middle"><%= item.title %></div></a></div></a></li><%}%>', {
					infoList: data
				})
			
				$("#zphg .videoBox .con-list").append(lis)
			});
			// 获取花絮
			list.getPlayer('1', function (res) {

				var data = res;

			
					var firstData = data.shift();
					var secData = data;
				
				

				$('#jmhx .FistFury-ShowTrivia-left img').attr('src', firstData.images)
				$('#jmhx  a').attr('href', '/fragment/' + firstData.id)
				$('#jmhx .FistFury-ShowTrivia-left .con-piclist-middle').text(firstData.title)


				var lis = ejs.render('<% for(var i=0, item; item = infoList[i++];){%>' +
						'<div class="swiper-slide">' +
							'<ul class="clearfix">' +
								'<% for(var j=0; j<item.length; j++){ %>' +
									'<li class="con-list-li">' +
									'<a href="/fragment/<%= item[j].id%>" target="_blank" style="position: absolute; top:0; left:0;">' +
										'<div class="con-piclist-pic">' +
											'<a href="/fragment/<%= item[j].id%>" target="_blank">' +
												'<img src="<%= item[j].images%>" alt="">' +
											'</a>' +
										'</div>' +
										'<div class="con-piclist-info">' +
											'<a href="/fragment/<%= item[j].id%>" class="con-piclist-title" target="_blank">' +
												'<div class="con-piclist-middle"><%= item[j].title %></div>' +
											'</a>' +
										'</div>' +
										'</a>' +
									'</li>' +
								'<%}%>' +
							'</ul>' +
						'</div>' +
					'<%}%>', {
						infoList: list.ArrayClassify(secData, 6)
					})
				$("#leftTabBox .swiper-wrapper").append(lis)

			});
			// 获取广告列表首页-正片回顾下方
			list.getAdverts('02', function (res) {
				$('#advertising a img').attr('src', res.adverts[0].photo);
				$('#advertising a').attr('href', res.adverts[0].url);
				if (res.show) {
					$('#advertising').show();
				} else {
					$('#advertising').hide();
				}
				if (res.adverts[0].show) {
					$('#advertising span').show();
				} else {
					$('#advertising span').hide();
				}
			});
			// 获取广告列表首页-微博右边
			list.getAdverts('01', function (res) {
				if(res.show){
					for (var a = 0; a < res.adverts.length; a++) {
						$('#wbry .weibo-right-Li img').eq(a).attr('src', res.adverts[a].photo);
						$('#wbry .weibo-right-Li .adLink').eq(a).attr('href', res.adverts[a].url);

						if (res.adverts[a].show) {							
							$('#wbry .weibo-right-Li .weiboad').eq(a).show();
						} else {
							$('#wbry .weibo-right-Li').eq(a).hide();
						}
	
						
					}
				}else{
					$('.weibo-right .weibo-right-List').hide()
				}
				
				
			});
			// 获取导师
			list.getPeople('01', function (res) {
				var redArr = [],
					blueArr = [];
				for (var index = 0; index < res.length; index++) {
					if (res[index].teamCode == 'red') {
						redArr.push(res[index])
					} else {
						blueArr.push(res[index])
					}
				}

				for (var i = 0; i < redArr.length; i++) {
					$('.fight_left .tutor li').eq(i).find('.gameImg').attr('src', redArr[i].photo)
					$('.fight_left .tutor li').eq(i).find('.gameHoverName').text(redArr[i].useName)
					$('.fight_left .tutor li').eq(i).find('.gameHoverSection').text(redArr[i].description)

					if(redArr[i].hrefUrl){
						$('.fight_left .tutor li').eq(i).attr('data-url', redArr[i].hrefUrl)
					}
					
				}
				for (var j = 0; j < blueArr.length; j++) {
					$('.fight_right .tutor li').eq(j).find('.gameImg').attr('src', blueArr[j].photo)
					$('.fight_right .tutor li').eq(j).find('.gameHoverName').text(blueArr[j].useName)
					$('.fight_right .tutor li').eq(j).find('.gameHoverSection').text(blueArr[j].description)
					if(blueArr[j].hrefUrl){
						$('.fight_right .tutor li').eq(j).attr('data-url', blueArr[j].hrefUrl)
					}
				}

			});
			// 普通选手
			list.getPeople('03', function (res) {
				var redArr = [],
					blueArr = [];
				for (var index = 0; index < res.length; index++) {
					if (res[index].teamCode == 'red') {
						redArr.push(res[index])
					} else {
						blueArr.push(res[index])
					}
				}

				for (var i = 0; i < redArr.length; i++) {
					if(redArr[i].hrefUrl){
						$('.fight_left .fight li').eq(i).attr('data-url', redArr[i].hrefUrl)
					}
					
					$('.fight_left .fight li').eq(i).find('.gameImg').attr('src', redArr[i].photo)
					$('.fight_left .fight li').eq(i).find('.gameHoverName').text(redArr[i].useName)
					$('.fight_left .fight li').eq(i).find('.nationalFlag').attr('src', redArr[i].countryUrl )
					$('.fight_left .fight li').eq(i).find('.gameHoverSection .ping').text(redArr[i].victoryCount || 0 + '胜-' + redArr[i].failCount || 0 + '负-' + redArr[i].tieCount + '平')
					$('.fight_left .fight li').eq(i).find('.gameHoverSection .kg').text(redArr[i].age + '岁/' + redArr[i].height + 'cm/' + redArr[i].weight + 'kg')
				}
				for (var j = 0; j < blueArr.length; j++) {
					if(blueArr[j].hrefUrl){
						$('.fight_right .fight li').eq(j).attr('data-url', blueArr[j].hrefUrl)
					}
					$('.fight_right .fight li').eq(j).find('.gameImg').attr('src', blueArr[j].photo)
					$('.fight_right .fight li').eq(j).find('.gameHoverName').text(blueArr[j].useName)
					$('.fight_right .fight li').eq(j).find('.nationalFlag').attr('src', blueArr[j].countryUrl)
					$('.fight_right .fight li').eq(j).find('.gameHoverSection .ping').text(blueArr[j].victoryCount || 0 + '胜-' + blueArr[j].failCount || 0 + '负-' + blueArr[j].tieCount + '平')
					$('.fight_right .fight li').eq(j).find('.gameHoverSection .kg').text(blueArr[j].age + '岁/' + blueArr[j].height + 'cm/' + blueArr[j].weight + 'kg')
				}
			});
			// 获取教练
			list.getPeople('02', function (res) {
				
					
					for (var i = 0; i < res.length; i++) {
						
						$('.mysterious li img').eq(i).attr('src', res[i].photo)						
						$('.mysterious li .con-piclist-middle').eq(i).text(res[i].useName)
						if(res[i].hrefUrl){
							$('.mysterious li').eq(i).attr('data-url', res[i].hrefUrl)
						}
					
					}
					
			});
			// 获取嘉宾
			list.getPeople('04', function (res) {
				for (var index = 0; index < res.length; index++) {
					$('.zhuanyejiaolian li').eq(index).find('img').attr('src', res[index].photo)
					$('.zhuanyejiaolian li').eq(index).find('.con-piclist-middle').text(res[index].useName)
					if(res[index].hrefUrl){
						$('.zhuanyejiaolian li').eq(index).attr('data-url', res[index].hrefUrl)
					}


				}
			});


			list.discern();

		},
		// 数组分类
		ArrayClassify: function (arr, len) {
			var a_len = arr.length;
			var result = [];
			for (var i = 0; i < a_len; i += len) {
				result.push(arr.slice(i, i + len));
			}

			return result;
		},
		// 获取选手（包含导师、教练、嘉宾与普通选手）
		getPeople: function(num, callback) {
			$.ajax({
				url: '/getPeople',
				type: 'post',
				data: {
					'playerType': num,
					'page': 1,
					'pageSize': 1000
				},
				success: function (data) {
					callback(data)

				}
			})
		},
		// 获取正片/花絮列表
		getPlayer: function(num, callback) {
			$.ajax({
				url: '/getPlayer',
				type: 'post',
				data: {
					'type': num,
					'page': 1,
					'pageSize': 1000
				},
				success: function (data) {
					callback(data)

				}
			})
		},
		//获取广告列表
		getAdverts: function(num, callback) {
			$.ajax({
				url: '/getAdverts',
				type: 'post',
				data: {
					'code': num
				},
				success: function (data) {

					callback(data)

				}
			})
		},
		// 视频点击获取对应播放来源
		player: function (myUrl) {
			// 下面的例子用来获取url的两个参数，并返回urlRewrite之前的真实Url
			setTimeout(function () {

				var reg = new RegExp('http://v.youku.com/v_show/id_([^.]*).html'),
					regs = new RegExp('https://v.youku.com/v_show/id_([^.]*).html'),
					url = myUrl;
				if (url.indexOf("https") != -1) {
					url = url.replace(regs, 'http://player.youku.com/embed/$1')
				} else {
					url = url.replace(reg, 'http://player.youku.com/embed/$1')
				}
				$('.iframe').attr('src', url + '?autoplay=0')
				// var reg = new RegExp('http://v.youku.com/v_show/id_([^.]*).html')
				// var regs = new RegExp('https://v.youku.com/v_show/id_([^.]*).html')
			
				// if (!!url) {
				// 	//判断传过来的是iframe标签还是网址链接
				// 	var getIframe = /^[ ]*<iframe/
				// 	var isIframe = getIframe.test(url)
				// 	if (isIframe) {
				// 		var replaceString = /'allowfullscreen'/
				// 		$('.iframe').html(url.replace(replaceString, ''))
				// 	} else {
				// 		//iframe标签 [图片]http://player.youku.com/embed/XMjYxNDYwODM1Mg==
				// 		if (url.indexOf("https") > -1) {
				// 			var a = url.replace(reg, 'https://player.youku.com/embed/$1');
				// 			if (regs.test(url)) {
				// 				$('.iframe').attr('src', a + '?autoplay=0')
				// 			}
				// 		} else {
				// 			var a = url.replace(reg, 'http://player.youku.com/embed/$1');
				// 			if (reg.test(url)) {
				// 				$('.iframe').attr('src', a + '?autoplay=0')
				// 			}
				// 		}
				// 	}
				// }
			}, 300)
		},
		// pc端或移动端处理逻辑
		discern: function () {
			if (/Android|webOS| iPhone | iPod |BlackBerry|opera mini|opera mobile|appleWebkit.*mobile|mobile/.test(
					navigator.userAgent)) {

				$('.img1').attr('src', '/index/images/h5/bg.png')
				$('.img2').attr('src', '/index/images/h5/bgtwo.png')
				$('.img3').attr('src', '/index/images/h5/jiabinzhengron.png')
				$('.img4').attr('src', '/index/images/h5/bgthree.png')
				$('.img4').css({
					width: 100 + '%',
					height: 16.333333 + 'rem'
				})
			} else {
				// pc端


				setTimeout(function () {
					var mySwiper = new Swiper('.swiper-container', {
						loop: false,
				
						// 如果需要分页器
						direction: 'horizontal',
						pagination: '.swiper-pagination',
						paginationClickable :true
					})

					btnshow($('#zphg .con-list .con-list-li').length, $('#zphg .list-slide-right'), $(
						'#zphg .list-slide-left'), $('#zphg .con-list'),900);
					btnshow($('#jczp .con-list .con-list-li').length, $('#jczp .list-slide-right'), $(
						'#jczp .list-slide-left'), $('#jczp .con-list'),917);
				}, 2000)


				// 点击回到顶部
				$(".fixed-right").click(function () {

					$(window).scrollTop(0);
					return false;
				});

				$(".headNavli").eq(0).on('click',function(){
					$(window).scrollTop(1065);
				})
				$(".headNavli").eq(1).on('click',function(){
					$(window).scrollTop(1695);
				})
				$(".headNavli").eq(2).on('click',function(){
					$(window).scrollTop(2425);
				})
				$(".headNavli").eq(3).on('click',function(){
					$(window).scrollTop(4045);
				})
				$(".headNavli").eq(4).on('click',function(){
					$(window).scrollTop(4575);
				})
				$(".headNavli").eq(5).on('click',function(){
					$(window).scrollTop(5315);
				})


				window.onscroll = function () {
					//获取当前的scrollTop
					var current_top = $(window).scrollTop();
				
					
					if (current_top > 1065) {
						$(".headNav").css({
							'position': 'fixed',
							'top': 96 + 'px',
							'left': 0,
							'z-index': 888,
							'width': 100 + '%'

						});
					} else {
						$(".headNav").css({
							'position': 'static',
							'width': '1003px'
						});
					}
				};


			}
		}

	}

	
	list.init();

	$('.dataList .item').on('click', function () {
		list.player($(this).attr('data-schemurl'));
		$(this).addClass('active').siblings().removeClass("active")
	})

	$('.headNavli').on('click', function () {
		$(this).addClass('selected').siblings().removeClass("selected")
	})

	$('#jbzr li').on('click', function(){
		if($(this).attr('data-url')){
			window.open($(this).attr('data-url'))
		}
	})



})(Zepto)