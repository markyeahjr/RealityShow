(function ($) {
	var $nav = $('#nav'),
		$searchBtn = $nav.find('.glyphicon-search'),
		$searchInput = $nav.find('.form-group input');

	var base = {
		init: function() {
			base.search();
			// 切换
			$('.collapsed').click(function() {
				$('.nav-mask').fadeToggle();
			})
			// 导航栏开启时禁止滚动
			$('.nav-mask,.navbar').on('touchmove', function (e) {
				e.preventDefault();
			})
			// 点击导航栏外区域隐藏导航栏
			$(document).on('click', function(e) {
				var contentEle = $('.navbar')
				//关闭遮罩层的代码或者需要在点击指定区域之外区域才执行的代码
					if (!$('.navbar-toggle').hasClass('collapsed')&&contentEle!== e.target && contentEle.has(e.target).length === 0) {
						$('.navbar-toggle').click();
					}
			});
			$('.navbar-nav li').click(function() {
				$(this).find('a').addClass('active');
				$(this).siblings().children('a').removeClass('active')
			})
		},
		search: function () {
			$searchBtn.click(function () {
				var url = '/search/?title=' + $(this).siblings('input').val();
				if (window.location.href.indexOf('/search/?') != -1) {
					window.location.href = url;
				} else {
					window.open(url);
				}
			})
			$searchInput.keydown(function(event) {
				if (event.keyCode == "13") {
					var url = '/search/?title=' + $(this).val();
					if (window.location.href.indexOf('/search/?') != -1) {
						window.location.href = url;
					} else {
						window.open(url);
					}
					event.preventDefault();
				}
				console.log(event.keyCode == "13");
			})
		}
	}
	$(function() {
		FastClick.attach(document.body);
		base.init();
	})
})(jQuery)