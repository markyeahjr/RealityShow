﻿var express = require('express');
var client = require('exceptionless').ExceptionlessClient.default;
var app = express();
var apis = apis || {};
apis.basketballApis = {};
exports.apis = apis;


// 当前环境（true:测试服、false:正式服）
var debug = true;

// 微信API接口地址
var wechatBaseUrl = 'https://api.weixin.qq.com/cgi-bin/';

// 健康猫微信公众号配置
apis.healthmallWechat = {
  appid: 'wx1cc3520d3eefeab7',
  appsecret: 'e0191ca260e2c1ddc68fd75c0ee2329c'
}

// “名校篮球对抗赛”微信公众号配置
apis.eubcWechat = {
  appid: debug ? 'wxaa6310deadcb85a7' : 'wx006cfaf69cc607fa',
  appsecret: debug ? '2c99b6495f3c2869dd9274e06fe6c668' : '5b156d7c7a16bdf5a863491198d6a9bf'
}

//EUBC 官方微博配置
apis.weibo = {
  appId: debug ? '2039206541' : '484169866',
  appSecret: debug ? 'c8badd8a01ed8cf240114399128dc518' : '756ff269c89f8fa67fff6bdb758158b3',
  //目前只有篮球投票对接了微博开放平台，此回调链接暂时写在这里
  redirectUrl: debug ? 'http://betadxact.healthmall.cn/basketballVote/activityIndex' : 'http://dxact.healthmall.cn/basketballVote/activityIndex'
}

//.net接口地址
apis.baseURL = debug ? 'http://beta-mma.cwmmma.com/realityShow/front' : 'http://mma.cwmmma.com/realityShow/front/';



//日志系统apikey
client.config.apiKey = 'zAJFsa0D6hRpcc4AHJP3GgybTXMkZDnR926klQXY';

var contentFormHeader = {
	'Content-Type': 'application/x-www-form-urlencoded',
};
var contentJsonHeader = {
	'Content-Type': 'application/json;charset=UTF-8',
};
var SSOFormHeader = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'appId': 101,
  'version': 3000000,
  'deviceId': 'h5',
  'versionName': '3.0.0',
  'deviceType': 3
};
var SSOFormHeaderForActivity = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'appId': 101,
	'version': 3000000,
	'deviceId': 'h5',
	'versionName': '3.0.0',
	'deviceType': 3
};
var applyActivity = {
	'Content-Type': 'application/x-www-form-urlencoded',
	'deviceType': 1,
	'versionCode': -1,
	'versionName': '1.0'

};


/* =================api================start */
apis.search = {
	get:{
	  method: "GET", headers: contentFormHeader
	}
};

apis.home = {
	// 选手（包含导师、教练、嘉宾与普通选手）
	getPlayer:{
		method: "GET", headers: contentFormHeader
	},
	// 获取正片回顾
	getVideos:{
		method: "GET", headers: contentFormHeader
	},
	//  获取广告列表
	getAdverts:{
		method: "GET", headers: contentFormHeader
	},
	//  获取合作伙伴
	getTypeList:{
		url: apis.baseURL + '/comm/typeList',method: "GET", headers: contentFormHeader
	},
	//  获取精彩照片
	getImages:{
		method: "GET", headers: contentFormHeader
	},
	//  获取资讯列表
	getNewsList:{
		method: "GET", headers: contentFormHeader
	},
	//  获取播放列表
	getPlaylists:{
		method: "GET", headers: contentFormHeader
	},

 }

apis.get = {
	method: "GET", headers: contentFormHeader
};
// 获取资讯列表
apis.getInfoList = {
	method: "GET", headers: contentFormHeader
};
// 获取资讯详情
apis.getInfoDetail = {
	method: "GET", headers: contentFormHeader
};
// 获取资讯详情
apis.getInfoAdverts = {
	url: apis.baseURL + '/comm/advert/03', method: "GET", headers: contentFormHeader
};
//  获取正片播放列表
apis.getPositiveList = {
	 method: "GET", headers: contentFormHeader
};
//  获取节目花絮列表
apis.getFragmentList = {
	method: "GET", headers: contentFormHeader
};
//  获取视频详情
apis.getVideoDetails = {
	method: "GET", headers: contentFormHeader
};
/* =================api================end */




