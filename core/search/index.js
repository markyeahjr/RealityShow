/*
 * @Author: hexiaotian 
 * @Date: 2018-04-08 16:02:02 
 * @Last Modified by: shenjr
 * @Last Modified time: 2018-08-01 15:02:07
 */


var apis = require("../api").apis;
var server = require('../common/server').server;
var querystring = require("querystring");
var search = {};
exports.search = search;

search.getSearchList = function (req, res, next) {

  var params ={
    title:  req.query.title,
    page: req.query.page,
    count: 10,
    tabType: req.query.type
  }
  
  apis.search.get.url = apis.baseURL + '/comm/search?' + querystring.stringify(params);
  console.log(apis.search.get.url);
  var option = {
		req: req,
		res: res,
		body: apis.search.get,
		type: "render",
		failCode: "responseCode",
		message: "获取查询列表",
		success: function (data) {
			req.body.data = {
        data: data.data,
        title: req.query.title,
        count: data.count,
      };
			return next();
		}
	}
	server.request(option);
}


search.getsearchDetail = function (req, res, next) {
  var params ={
    id: req.params.id,
  }
  console.log(JSON.stringify(params));
  
  req.body.data = {
    title: '企业投资项目核准和备案管理办法是怎么规定的',
    id: req.params.id
  }
  return next();
  // var option = {
	// 	req: req,
	// 	res: res,
	// 	body: apis.mewDouRedPacket.getInitInfo,
	// 	type: "json",
	// 	failCode: "responseCode",
	// 	message: "获取红包活动详情",
	// 	success: function (data) {
	// 		req.body.initInfo = data.data;
	// 		return next();
	// 	}
	// }
	// server.request(option);
}