/*
 * @Author: hexiaotian 
 * @Date: 2018-04-08 16:02:02 
 * @Last Modified by: mikey.zhaopeng
 * @Last Modified time: 2018-08-15 15:08:04
 */


var apis = require("../api").apis;
var server = require('../common/server').server;
var querystring = require("querystring");
var home = {};
exports.home = home;

// 选手（包含导师、教练、嘉宾与普通选手
home.getPlayer = function (req, res, next) {
  var params ={
    page: req.body.page || 1,
    pageSize: req.body.pageSize || 1000,
    playerType: req.body.playerType || '01'
  }
 
  apis.home.getPlayer.url = apis.baseURL + '/players?' + querystring.stringify(params);
  var option = {
		req: req,
		res: res,
		body: apis.home.getPlayer,
		type: "render",
		failCode: "responseCode",
		message: "选手（包含导师、教练、嘉宾与普通选手",
		success: function (data) {
      req.body.getPlayer = data;
      
			return next();
		}
	}
	server.request(option);
}
 //获取正片/花絮列表
home.getVideos = function (req, res, next) {
  
  var params ={
    page: req.body.page || 1,
    pageSize: req.body.pageSize || 1000
  }
  var type = req.body.type || 0



	apis.home.getVideos.url =  apis.baseURL + '/info/video/' + type + '?' + querystring.stringify(params);
  var option = {
		req: req,
		res: res,
		body: apis.home.getVideos,
		type: "render",
		failCode: "responseCode",
		message: "获取正片/花絮列表",
		success: function (data) {
      req.body.getVideos = data;
      
			return next();
		}
	}
	server.request(option);
}


 //获取广告列表
 home.getAdverts = function (req, res, next) {
	
  var code = req.body.code || '01'

	apis.home.getAdverts.url = apis.baseURL + '/comm/advert/' + code;
	
  var option = {
		req: req,
		res: res,
		body: apis.home.getAdverts,
		type: "render",
		failCode: "responseCode",
		message: "获取广告列表",
		success: function (data) {
      
      req.body.getAdverts =data;
      
			return next();
		}
	}
	server.request(option);
}

//获取合作伙伴
home.getTypeList = function (req, res, next) {
  
	
  var option = {
		req: req,
		res: res,
		body: apis.home.getTypeList,
		type: "render",
		failCode: "responseCode",
		message: "获取合作伙伴",
		success: function (data) {
      
      req.body.getTypeList = data;
      
			return next();
		}
	}
	server.request(option);
}

//获取精彩照片
home.getImages = function (req, res, next) {
	
	var params ={
    page: req.query.page || 1,
    pageSize: req.query.pageSize || 1000
	}
	
	apis.home.getImages.url = apis.baseURL + '/info/images?' + querystring.stringify(params);
  var option = {
		req: req,
		res: res,
		body: apis.home.getImages,
		type: "render",
		failCode: "responseCode",
		message: "获取精彩照片",
		success: function (data) {
      
      req.body.getImages = data;
      
			return next();
		}
	}
	server.request(option);
}
//获取资讯列表
home.getNewsList = function (req, res, next) {
	
	var params ={
    page: req.query.page || 1,
    pageSize: req.query.pageSize || 1000
	}
	
	apis.home.getNewsList.url = apis.baseURL + '/info/news/list?' + querystring.stringify(params);
  var option = {
		req: req,
		res: res,
		body: apis.home.getNewsList,
		type: "render",
		failCode: "responseCode",
		message: "获取资讯列表",
		success: function (data) {
			var myData = data
	
			var firstData = myData.shift();


					secData = myData.slice(0,3);;
      
      req.body.getNewsList = firstData;
      req.body.getNewsLists = secData;
      
			return next();
		}
	}
	server.request(option);
}
//获取播放列表
home.getPlaylists = function (req, res, next) {
	

	apis.home.getPlaylists.url =	apis.baseURL + '/info/playlists';
  var option = {
		req: req,
		res: res,
		body: apis.home.getPlaylists,
		type: "render",
		failCode: "responseCode",
		message: "获取播放列表",
		success: function (data) {
			req.body.getPlaylists =data;
      
			return next();
		}
	}
	server.request(option);
}

