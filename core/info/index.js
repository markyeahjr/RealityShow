/*
 * @Author: hexiaotian 
 * @Date: 2018-04-08 16:02:02 
 * @Last Modified by: hexiaotian
 * @Last Modified time: 2018-07-31 11:01:33
 */


var apis = require("../api").apis;
var server = require('../common/server').server;
var querystring = require("querystring");

var info = {};
exports.info = info;

info.getInfoList = function (req, res, next) {
  

  var params ={
    page: req.query.page,
    count: 10
  }
  console.log(req.query);
  apis.getInfoList.url = apis.baseURL + '/info/news/list?' + querystring.stringify(params);
  console.log(apis.getInfoList.url);
  var option = {
		req: req,
		res: res,
		body: apis.getInfoList,
		type: "render",
		failCode: "responseCode",
		message: "获取资讯列表",
		success: function (data) {
			req.body.data = {
        data: data,
      };
			return next();
		}
	}
	server.request(option);
}

// 资讯详情
info.getInfoDetail = function (req, res, next) {

  apis.getInfoDetail.url = apis.baseURL + '/info/news/' + req.params.id;
  console.log(apis.getInfoDetail.url);
  
  var option = {
		req: req,
		res: res,
		body: apis.getInfoDetail,
		type: "render",
		failCode: "responseCode",
		message: "获取资讯详情",
		success: function (data) {
			// console.log(data);
			req.body.data = data;
			return next();
		}
	}
	server.request(option);
}

//获取资讯列表广告
info.getInfoAdverts = function (req, res, next) {
	
  var option = {
		req: req,
		res: res,
		body: apis.getInfoAdverts,
		type: "render",
		failCode: "responseCode",
		message: "获取资讯列表广告",
		success: function (data) {
      req.body.adverts = data;
			return next();
		}
	}
	server.request(option);
}