/*
 * @Author: hexiaotian 
 * @Date: 2018-04-08 16:02:02 
 * @Last Modified by: shenjr
 * @Last Modified time: 2018-08-16 16:27:17
 */


var apis = require("../api").apis;
var server = require('../common/server').server;
var querystring = require("querystring");
var positive = {};
exports.positive = positive;

positive.getPositiveList = function (req, res, next) {
	

	apis.getPositiveList.url = apis.baseURL + '/info/video/0?page=1&pageSize=300';

  var option = {
		req: req,
		res: res,
		body: apis.getPositiveList,
		type: "render",
		failCode: "responseCode",
		message: "获取播放列表",
		success: function (data) {
			req.body.data.data = data;
			// if (!req.params.id && data.length) {
			// 	req.body.data.details = data[0] || {};
			// } else if (req.body.data.details) {
			// 	req.body.data.data.unshift(req.body.data.details);
			// }
			// 替换详情视频播放地址
			var reg = new RegExp('http://v.youku.com/v_show/id_([^.]*).html'),
			regs = new RegExp('https://v.youku.com/v_show/id_([^.]*).html'),
			url = req.body.data.details.hrefUrl;
			if (url.indexOf("https") != -1) {
				url = url.replace(regs, 'http://player.youku.com/embed/$1')
			} else {
				url = url.replace(reg, 'http://player.youku.com/embed/$1')
			}
			req.body.data.details.hrefUrl = url + '?autoplay=1';

			return next();
		}
	}
	server.request(option);
}


positive.getPositiveDetails = function (req, res, next) {
	
	req.body.data = {};
	if (!req.params.id) {
		return next();
	}
	apis.getVideoDetails.url = apis.baseURL + '/info/video/detail/' + req.params.id;
  console.log(apis.getVideoDetails.url);
  var option = {
		req: req,
		res: res,
		body: apis.getVideoDetails,
		type: "render",
		failCode: "responseCode",
		message: "获取视频详情",
		success: function (data) {
			req.body.data.details = data;
		
			return next();
		}
	}
	server.request(option);
}