﻿/**
 * 此文件封装了公共的node方法。
*/

var global = global || {};
exports.global = global;

//合并json，将jsonB合并到jsonA的name字段下面
//name为新字段的名称，aID为主jsonA里的某个id字段，jsonA为主json，jsonB为将被合并的json，bID为jsonB的ID字段，bKey为jsonB里的名称字段
global.mergeJson = function (name, aID, jsonA, jsonB, bID, bKey){
    var jsonALength=jsonA.length, jsonBLength=jsonB.length;
    if (jsonALength) {
        for(var i=0;i<jsonALength;i++){
            for(var j=0;j<jsonB.length;j++){
                if(jsonA[i][aID]==jsonB[j][bID]){
                    jsonA[i][name]=jsonB[j];
                }
            }
        }
    }
    return jsonA;
}

//根据ID获取名字
global.getNameByID = function (ID, targetJson, targetID, targetName){
    for(var i=0; i<targetJson.length; i++){
        if(targetJson[i][targetID]==ID){
            return targetJson[i][targetName];
        }
    }
}

//生成随机字符串，如果第二次参数不为null，则生成随机的纯数字串，否则生成随机的字符串。
global.generateRandomAlphaNum = function (len,radix) {
    radix = radix ? 10 : 36;
    var rdmString = "";
    for (; rdmString.length < len; rdmString += Math.random().toString(radix).substr(2));
    return rdmString.substr(0, len);
}