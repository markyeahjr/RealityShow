﻿/**
 * 此文件封装了日志系统的方法，通过调用writeLog即可写入日志。
*/

var log = {};
var client = require('exceptionless').ExceptionlessClient.default;
client.config.serverUrl = "http://log.healthmall.cn";
client.config.apiKey = 'zAJFsa0D6hRpcc4AHJP3GgybTXMkZDnR926klQXY';
client.config.useDebugLogger();
client.config.useLocalStorage();

// type支持Debug、Info、Warn、Error、Fatal五种类型
client.writeLog = function(type, req, msg, params, res){
    var host = req.headers.host;
    if(host.indexOf("192.168") === -1){
        //本地调试，不写日志
        var env = host.indexOf("beta") !== -1 ? "beta": "prod";
        var t = client.createLog(env, msg, type).addRequestInfo(req);
        t.setProperty("params", params);
        t.setProperty("response", res);
        t.submit();
    }
}
exports.log = client;