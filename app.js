var express = require('express');
var moment = require('moment');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var app = express();

var shortDateFormat = 'ddd @ h:mmA'; 
app.locals.moment = moment; 
app.locals.shortDateFormat = shortDateFormat; 
// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.engine('.html', ejs.__express);
app.set('view engine', 'html');
app.set("view options",{
    "layout":false
});
// uncomment after placing your favicon in /public
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.urlencoded({ limit: 1024 * 1024 * 1024 ,extended: true }));
app.use(bodyParser({
    limit: 1024 * 1024 * 1024 ,// 64M
    extended: true
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

/********************************路由配置********************************/

//首页
var index = require('./routes/index/index.js');
app.use('/', index);

//查找
var search = require('./routes/search/index.js');
app.use('/search', search);

//资讯
var info = require('./routes/info/index.js');
app.use('/info', info);

//正片回顾
var positive = require('./routes/positive/index.js');
app.use('/positive', positive);

//节目花絮
var fragment = require('./routes/fragment/index.js');
app.use('/fragment', fragment);


/********************************路由配置 End********************************/

// catch statusCode and show
app.use('/getStatus', function(req, res, next) {
	res.send(res.statusCode + '');
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
module.exports = app;

