项目简介(test)
========
真人秀官网项目。

启动方式
========
1. 在根目录运行`npm install`安装NodeJS依赖模块
2. 在根目录运行`npm start`启动服务
3. 在浏览器打开 [http://localhost:8010/](http://localhost:8010/) 


项目结构
========
注：core、public、routes、views下每一个文件夹对应一个模块

- /bin/www —— 端口配置文件
- /common —— 日志配置，错误配置等文件
- /core —— 中间件文件夹
	- /core/common —— 公共中间件
	- /core/fragment —— 节目花絮相关中间件
	- /core/index —— 首页相关中间件
	- /core/info —— 资讯相关中间件
	- /core/positive —— 正片相关中间件
	- /core/search —— 搜索相关中间件
	- /core/api.js —— 接口地址配置文件
- /node_modules —— nodejs依赖模块
- /public/ —— 静态文件夹
	- /public/common —— 公共静态文件
	- /public/fragment —— 节目花絮相关静态文件
	- /public/index —— 首页相关静态文件
	- /public/info —— 资讯相关静态文件
	- /public/positive —— 正片相关静态文件
	- /public/search —— 搜索相关静态文件
- /routes/ —— 路由配置
	- /routes/common —— 公共中间件的路由配置
	- /routes/fragment —— 节目花絮相关路由配置
	- /routes/index —— 首页相关路由配置
	- /routes/info —— 资讯相关路由配置
	- /routes/positive —— 正片相关路由配置
	- /routes/search —— 搜索相关路由配置
- /views/ —— 视图文件夹
	- /views/fragment —— 节目花絮相关视图
	- /views/index —— 首页相关视图
	- /views/info —— 资讯相关视图
	- /views/positive —— 正片相关视图
	- /views/search —— 搜索相关视图
	- /views/template —— 视图模板
	- /views/error.html —— 错误视图

路由规则
========
如：http://dxact.healthmall.cn/mall/20160925?type=trainer
```
    dxact.healthmall.cn —— 域名部分
				   mall —— 模块
			   20160925 —— 活动日期
			   	   type —— 其他参数
```

上线前需准备工作
========
1. 测试服接口地址换成正式服
2. 提供出去的链接，带上程序所需参数
3. 所有的token都是动态获取的
4. 和app有交互时，确保是动态读取app cookies
5. 去掉前端JS的debugger，console，注释等不必要的代码
7. 端口号和服务器所开的端口号一致
8. 将api.js第三方appid和appsecret改成正式的



